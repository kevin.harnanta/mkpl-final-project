INSERT INTO `akun` (`id_akun`, `nama_akun`, `email_akun`, `password_akun`) VALUES
('5ty', 'aaa', 'bbb@b', 'a'),
('68lXJrLFjR', 'aaa', 'bbb@b', 'xyz'),
('EThhzPCDD1', 'test', 'test@test', 'test'),
('EUw2duf4cK', 'aaa', 'bbb@b', 'a'),
('O7aYZnlaEk', 'aaaa', 'aaa@a', 'aa'),
('oCPhcQvSzd', 'jojo', 'jojo@jojo.com', 'jojo'),
('R3keHOJ8sa', 'aaaa', 'aaa@a', 'aa'),
('xMtq2gc7ZS', 'aaaa', 'admin@a', 'abc'),
('56Ethho6aY', 'leoj', 'leoj@leoj.leoj', 'leoj'),
('dTwy3q47cT', 'joel', 'joel@joel.joel', 'joel');

INSERT INTO `forum` (`id_forum`, `id_akun`, `topik`, `deskripsi`, `waktu`, `favorite`) VALUES
('ar1aravxVU', 'oCPhcQvSzd', 'Petualangan', 'Gimana nih berpetualang?', '2019-11-22 07:52:53', '1'),
('C23J1K6Uqz', 'EThhzPCDD1', 'tettttt', 'testststs', '2019-11-20 13:00:11', NULL),
('QaS89oUZgw', 'EThhzPCDD1', 'te', 'te', '2019-11-20 13:00:40', NULL),
('qhPLC4LFjX', 'EThhzPCDD1', 'Programming', 'Gimana sih cara belajar Java?', '2019-11-23 04:02:42', '0'),
('test', '68lXJrLFjR', 'test topik', 'Ini adalah test deskripsi pertama', '2019-11-16 12:19:37', '0'),
('test2', 'xMtq2gc7ZS', 'test2 ', 'test2 deskripsi ', '2019-11-16 17:00:00', NULL),
('test3', 'xMtq2gc7ZS', 'test yang ketiga', 'test ketiga', '2019-11-16 17:16:04', NULL);

INSERT INTO `posting` (`id_reply`, `id_forum`, `deskripsi`, `waktu`, `id_akun`) VALUES
('gKu2ggScv1', 'ar1aravxVU', 'Test', '2019-11-22 10:51:12', 'oCPhcQvSzd'),
('H0mzkFAQeA', 'qhPLC4LFjX', 'Apa kabar?', '2019-11-23 04:48:46', 'oCPhcQvSzd'),
('kReoEP5gmV', 'ar1aravxVU', 'A', '2019-11-22 10:52:43', 'oCPhcQvSzd'),
('lKDnPOV7ts', 'ar1aravxVU', 'Halo!', '2019-11-22 10:48:46', 'oCPhcQvSzd'),
('Lt023ozwB9', 'qhPLC4LFjX', '', '2019-11-23 04:46:42', 'oCPhcQvSzd'),
('nJGJnfsvcI', 'qhPLC4LFjX', 'Gimana ya', '2019-11-23 04:46:03', 'oCPhcQvSzd'),
('nSBWbixBZz', 'ar1aravxVU', 'XX', '2019-11-22 10:52:13', 'oCPhcQvSzd'),
('PUYVeRrWij', 'test', 'Test Komentar Pertama dari Akun Lain', '2019-11-22 07:52:17', 'oCPhcQvSzd'),
('qhjVMKhp6k', 'qhPLC4LFjX', 'Baik2 saja', '2019-11-23 04:49:31', 'oCPhcQvSzd'),
('U5NXnfNzoz', 'test', 'Test Komentar Kedua', '2019-11-22 07:24:40', 'EThhzPCDD1'),
('v5pU6Bh9kI', 'ar1aravxVU', 'Yeah!', '2019-11-22 07:59:23', 'oCPhcQvSzd'),
('Y42POxQ0LQ', 'ar1aravxVU', 'Hai', '2019-11-24 14:50:54', 'EThhzPCDD1'),
('YlJCDmvb0P', 'test', 'Test Komentar Pertama', '2019-11-22 06:39:30', 'EThhzPCDD1');

INSERT INTO `tbl_forum_suka_tidak` (`id`, `id_forum`, `id_akun`, `suka`, `tidak_suka`) VALUES
('1e5FpPtzHweAp1bjJWsy', 'qhPLC4LFjX', 'EThhzPCDD1', '1', '0'),
('2xJfPmYIY4y2bEf8LL6v', 'qhPLC4LFjX', 'EThhzPCDD1', '0', '1'),
('3EBattMpqbpAWYVkgdrF', 'qhPLC4LFjX', 'EThhzPCDD1', '1', '0'),
('Af3635h0yhR9dzOj8nNA', 'qhPLC4LFjX', 'EThhzPCDD1', '0', '1'),
('B2kMPrEDLM52S5eT9yzy', 'qhPLC4LFjX', 'EThhzPCDD1', '1', '0'),
('flA5AwkVojkMBpz9lUWh', 'qhPLC4LFjX', 'EThhzPCDD1', '0', '1'),
('QtYHtQgPmvj6AyglRfxx', 'ar1aravxVU', 'EThhzPCDD1', '0', '1'),
('test', 'ar1aravxVU', 'oCPhcQvSzd', '1', '0'),
('test2', 'ar1aravxVU', '5ty', '0', '1'),
('uZuxvetBSnlUHIRwGtrq', 'qhPLC4LFjX', 'EThhzPCDD1', '1', '0'),
('z8ipMXZSMo2sXgWTqc2M', 'qhPLC4LFjX', 'EThhzPCDD1', '1', '0');
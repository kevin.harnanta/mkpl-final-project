CREATE TABLE `akun` (
  `id_akun` varchar(10) NOT NULL,
  `nama_akun` varchar(255) NOT NULL,
  `email_akun` varchar(255) NOT NULL,
  `password_akun` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `forum` (
  `id_forum` varchar(10) NOT NULL,
  `id_akun` varchar(10) NOT NULL,
  `topik` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `waktu` datetime NOT NULL,
  `favorite` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `posting` (
  `id_reply` varchar(10) NOT NULL,
  `id_forum` varchar(10) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `waktu` datetime NOT NULL,
  `id_akun` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_forum_suka_tidak` (
  `id` varchar(20) NOT NULL,
  `id_forum` varchar(10) NOT NULL,
  `id_akun` varchar(10) NOT NULL,
  `suka` enum('0','1') DEFAULT NULL,
  `tidak_suka` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`);

ALTER TABLE `forum`
  ADD PRIMARY KEY (`id_forum`),
  ADD KEY `id_akun` (`id_akun`);

ALTER TABLE `posting`
  ADD PRIMARY KEY (`id_reply`);

ALTER TABLE `tbl_forum_suka_tidak`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `forum`
  ADD CONSTRAINT `forum_ibfk_1` FOREIGN KEY (`id_akun`) REFERENCES `akun` (`id_akun`);
COMMIT;
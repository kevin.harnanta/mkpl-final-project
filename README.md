# mkpl-final-project

## How to run this project

```
$ git clone https://github.com/guaychou/mkpl-final-project.git
$ cd mkpl-final-project
$ docker-compose up
```

### Version

If you want to run with specific version of docker image please visit: https://hub.docker.com/repository/docker/lordchou/mkpl-project for available project image tag, and https://hub.docker.com/repository/docker/lordchou/mkpl-database for available project database tag.

After you visit link above you can change the docker-compose.yml file, but for our stability please use latest tag instead of number version tag because this project exist because our lecture advocate us to create bug.

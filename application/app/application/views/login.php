<?php /*session_start(); // Start session nya
// Kita cek apakah user sudah login atau belum
// Cek nya dengan cara cek apakah terdapat session username atau tidak
if(isset($_SESSION['nama_akun'])){ 
	// Jika session username ada berarti dia sudah login  
	redirect("User/index"); // Kita Redirect ke halaman welcome.php
}*/

if($this->session->userdata('nama_akun') != null) {
	redirect('User/index');
}
?>

<html lang="en">

<head>
	<meta charset="UTF-8">

	<title>Login | Register</title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login.css">

</head>

<body>
	<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
		<h5 class="my-0 mr-md-auto font-weight-normal">MKPL</h5>
		<nav class="my-2 my-md-0 mr-md-3">
			<a class="p-2 text-dark" href="<?php echo site_url('Home/index'); ?>">Home</a>
			<a class="p-2 text-dark" href="#">About</a>
		</nav>
		<a class="btn btn-outline-primary" href="<?php echo site_url('Home/index'); ?>">Login</a>
	</div>

	<div class="cont">

		<div class="form sign-in">
			<form action="<?php echo site_url('Home/checkAccount'); ?>" method="post">
				<h2>Selamat Datang Kembali</h2>
				<label>
					<span>Email</span>
					<input type="email" name="email_akun" placeholder="email" required>
				</label>
				<label>
					<span>Password</span>
					<input type="password" name="password_akun" placeholder="password" required>
				</label>
				<button type="submit" class="submit">Login</button>
			</form>
		</div>
		<div class="sub-cont">
			<div class="img">
				<div class="img__text m--up">
					<h2>Anda <b>pengguna baru?</b></h2>
				</div>
				<div class="img__text m--in">
					<h2>Sudah pernah buat akun?</h2>
				</div>
				<div class="img__btn">
					<span class="m--up">Register</span>
					<span class="m--in">Login</span>
				</div>
			</div>
			<form action="<?php echo site_url('Home/insert_akun'); ?>" method="post">
				<div class="form sign-up">
					<h2>Registrasi</h2>
					<label>
						<span>Nama</span>
						<input type="text" name="nama_akun" placeholder="nama" required>
					</label>
					<label>
						<span>Email</span>
						<input type="email" name="email_akun" placeholder="email" required>
					</label>
					<label>
						<span>Password</span>
						<input type="password" name="password_akun" placeholder="password" required>
					</label>
					<button type="submit" class="submit" value="Sign Up">Sign Up</button>
			</form>
		</div>
	</div>
	</div>

	<script id="rendered-js" src="<?php echo base_url(); ?>assets/js/login.js"></script>

</body>

</html>

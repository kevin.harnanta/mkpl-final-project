<?php
/*session_start(); 
// Start session nya
// Kita cek apakah user sudah login atau belum
// Cek nya dengan cara cek apakah terdapat session username atau tidak
if( ! isset($_SESSION['username'])){ // Jika tidak ada session username berarti dia belum login  
redirect('Home/index'); // Kita Redirect ke halaman index.php karena belum login
}*/

?>

<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Lihat Balasan</title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/login.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/user.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/reply.css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
		integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>
	<div
		class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow fixed-top">
		<h5 class="my-0 mr-md-auto font-weight-normal">Selamat Datang,
			<?php echo $this->session->userdata('nama_akun'); ?></h5>
		<nav class="my-2 my-md-0 mr-md-3">
			<a class="p-2 text-dark" href="#About" data-toggle="modal">About</a>
		</nav>
		<a class="btn btn-outline-primary" href="<?php echo site_url('User/logout'); ?>">Logout</a>
	</div>

	<div id="left-sidebar">
		<div class="m-3">
			<button type="button" class="btn btn-info mb-3" data-toggle="modal" data-target=".bd-example-modal-lg">
				<i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp Mulai Diskusi</button>
			<table>
				<tbody>
					<tr>
						<td id="table_icon"><i class="fa fa-home"></i></td>
						<td id="table_desc"><a href="<?php echo site_url('User/index'); ?>">Beranda</a></td>
					</tr>
					<tr>
						<td id="table_icon"><i class="fa fa-comment"></i></td>
						<td id="table_desc"><a href="<?php echo site_url('User/index'); ?>">Semua Diskusi</a></td>
					</tr>
					<tr>
						<td id="table_icon"><i class="fa fa-star"></i></td>
						<td id="table_desc"><a href="<?php echo site_url('User/daftarFavorit'); ?>">Daftar Favorit</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal About -->
	<div class="modal fade" id="About" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-info"></i>&nbsp About App</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">You have a forum?
Make your forum 100% free to access
this web is a free web that makes your navigation friendly. You are an admin? Welcome to THIS app forum ! Add your forum in a few minutes and browse online discussions. Your forum is now available to your members and to the Forum App Community
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><i
							class="fa fa-times"></i>&nbsp Tutup</button>
				</div>
			</div>
		</div>
	</div>

	<!--New Modal Diskusi-->
	<div class="modal fade bd-example-modal-lg text-left" tabindex="-1" role="dialog"
		aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="mulaiDiskusi"><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp
						Apa nih?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="<?php echo site_url('User/insertForum'); ?>" method="POST">
						<div class="form-group">
							<label for="formGroupExampleInput">Topik</label>
							<input type="text" class="form-control" id="formGroupExampleInput"
								placeholder="Topik yang dibahas..." name="topic" required>
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Diskusi</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="desc"
								required></textarea>
						</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><i
							class="fa fa-times"></i>&nbsp
						Tutup</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-share"></i>&nbsp Kirim ke
						forum</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div id="content">
		<div class="m-3">
			<ul class="friend-list">
				<?php foreach ($getForum as $row) { 
					$id_forum = $row['id_forum']; ?>
				<table>
					<tbody>
						<tr>
							<td><img src="https://bootdey.com/img/Content/user_2.jpg" alt="user" class="img-circle">
							</td>
							<td style="width: 800px;">
								<div class="friend-name"><strong>&nbsp<?php echo $row['nama_akun']; ?></strong></div>
								<div>&nbsp<span class="badge badge-secondary"><?php echo $row['topik']; ?></span></div>
								<div class="last-message text-muted">&nbsp<?php echo $row['deskripsi']; ?></div>
							</td>
							<td>
								<p style="text-align: right"><small class="time text-muted"><?php
							
							$now = date('Y-m-d H:i:s');

							$datetime1 = date_create($row['waktu']);
							$datetime2 = date_create($now);
							
							$interval = date_diff($datetime1, $datetime2);

							    $time = new DateTime($row['waktu']);
                                $timediff = $time->diff(new DateTime());
                                
								echo $interval->format("%h"). " jam " . $interval->format("%i"). " menit lalu";
								?></small> <small class="chat-alert text-muted"><i class="fa fa-check"></i></p></small>
							</td>
						</tr>
						<tr>
							<td><strong>&nbsp<?php echo $row['nama_akun']; ?></strong></td>
						</tr>
						<tr>
							<td><a class="btn btn-sm btn-info" href="#btn-reply" id="btn-reply"><i
										class="fa fa-reply"></i>&nbsp Reply</a></td>
							<td></td>
							<td>

								<div id="likes_unlikes">
									&nbsp<a class="btn btn-sm btn-primary" disabled="disabled"
										href="<?php echo site_url('User/setLikes/').$row['id_forum']; ?>"
										id="btn-like"><?php foreach($getLikes as $likes) { echo $likes["total_suka"]; } ?>
										Likes</a></span>
									&nbsp<a class="btn btn-sm btn-danger"
										href="<?php echo site_url('User/setUnlikes/').$row['id_forum']; ?>"
										id="btn-unlike"><?php foreach($getUnlikes as $unlikes) { echo $unlikes["total_tidak_suka"]; } ?>
										Unlikes</a>
									&nbsp<a class="btn btn-sm btn-warning"
										href="<?php echo site_url('User/setFavorit/').$row['id_forum']; ?>"
										id="btn-favorit"><i class="fa fa-star"></i>&nbsp Favorite</a></div>
		</div>
		</td>
		</tr>
		</tbody>
		</table>
		<?php } ?>
		</ul>

		<h5> Komentar Saat Ini</h5>
		<ul class="friend-list">
			<?php foreach ($reply as $row) { $id_forum = $row['id_forum']; ?>
			<table>
				<tbody>
					<tr>
						<td><img src="https://bootdey.com/img/Content/user_2.jpg" alt="user" class="img-circle">
						</td>
						<td style="width: 800px;">
							<div class="friend-name"><strong>&nbsp<?php echo $row['nama_akun']; ?></strong></div>
							<div class="last-message text-muted">&nbsp<?php echo $row['deskripsi']; ?></div>
						</td>
						<td style="width: 267.25px;">
							<p style="text-align: right"><small class="time text-muted"><?php
							
							$now = date('Y-m-d H:i:s');

							$datetime1 = date_create($row['waktu']);
							$datetime2 = date_create($now);
							
							$interval = date_diff($datetime1, $datetime2);

							    $time = new DateTime($row['waktu']);
                                $timediff = $time->diff(new DateTime());
                                
								echo $interval->format("%h"). " jam " . $interval->format("%i"). " menit lalu";
								?></small> <small class="chat-alert text-muted"><i class="fa fa-check"></i></p></small>
						</td>
					</tr>
					<tr>
						<td><strong>&nbsp<?php echo $row['nama_akun']; ?></strong></td>
					</tr>
				</tbody>
			</table>
			<?php } ?>
		</ul>

		<script>
			$(document).ready(function () {
				$("#btn-reply").click(function () {
					$('#comment').append(`<form action="<?php echo site_url('User/insertReply/').$id_forum; ?>" method="POST">
			<div class="form-group">
				<label for="exampleFormControlTextarea1">Komentari</label>
				<textarea class="form-control" id="textArea" name="desc_reply" rows="3" required></textarea>
			</div>
			<button type="button" class="btn btn-sm btn-secondary" id="tutup">Tutup</button>
			<button type="submit" class="btn btn-sm btn-primary">Submit</button>
		</form>`);
					$("#btn-reply").remove();
				});
			});

		</script>
		<div id="comment">
		</div>
	</div>

	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
	</script>
</body>

</html>

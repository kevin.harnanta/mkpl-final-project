<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('login');
		$this->load->library('session');
	}

	public function getTest() {
		echo "test";
	}

	public function insert_akun() {
		$id_akun = $this->generateID();
		$nama_akun = $this->input->post('nama_akun');
		$email_akun = $this->input->post('email_akun');
		$password_akun = $this->input->post('password_akun');

		$data = array(
			'id_akun' => $id_akun,
			'nama_akun' => $nama_akun,
			'email_akun' => $email_akun,
			'password_akun' => $password_akun
		);

		$this->load->model('Model_home');
		$query = $this->Model_home->insertAccount($data);
		if ($query) {
			echo '<script type="text/javascript">alert("Data berhasil dimasukkan!");</script>';
			redirect('Home/index');
		}
		else {
			echo '<script type="text/javascript">alert("Data tidak berhasil dimasukkan!");</script>';
			redirect('Home/index');
		}
	}

	public function generateID() {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 10; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	#Tambah Cek Akun
	public function checkAccount() {

		$email = $this->input->post('email_akun');
		$password = $this->input->post('password_akun');

		$this->load->model('Model_home');
		$query = $this->Model_home->getAccount();

		foreach ($query->result() as $row) {
			if ($email == $row->email_akun && $password == $row->password_akun) {
				$this->session->set_userdata('nama_akun', $row->nama_akun);
				$this->session->set_userdata('id_akun', $row->id_akun);
				$ketemu = true;
				break;
			}
			else {
				echo false;
				$ketemu = false;
			}
		}

		if ($ketemu == true) {
			print_r($this->session->userdata());
			redirect('User/index');
		}
		else {
			echo '<script type="text/javascript">alert("Data tidak ada di database!");</script>';
			redirect('Home/index');
		}

	}
}

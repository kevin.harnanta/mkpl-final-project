<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function index() {
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->model('Model_user');
        $data['forum'] = $this->Model_user->getForum();
        $this->load->view('user', $data);
    }

    public function insertForum() {
        $id_forum = $this->generateID(10);
        $topik = $this->input->post('topic');
        $deskripsi = $this->input->post('desc');
        $datetime = date('Y-m-d H:i:s');

        $data = array(
            'id_forum' => $id_forum,
            'id_akun' => $this->session->userdata('id_akun'),
            'topik' => $topik,
            'deskripsi' => $deskripsi,
            'waktu' => $datetime
        );

        $this->load->model('Model_user');
        $query = $this->Model_user->insertForum($data);
		if ($query) {
			echo '<script type="text/javascript">alert("Data berhasil dimasukkan!");</script>';
			redirect('User/index');
		}
		else {
            echo '<script type="text/javascript">alert("Data tidak berhasil dimasukkan!");</script>';
            redirect('User/index');
		}
        
    }

    public function generateID($length) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
    }

    public function countDiff($datecurrent, $datepast) {
        $diff = date_diff($datecurrent, $datepast);
        return $diff;
    }

    public function logout() {
        $this->session->unset_userdata('nama_akun');
        $this->session->unset_userdata('id_akun');
        redirect('Home/index');
    }

    #Dapatkan Reply
    public function getReply($id) {
        $this->load->model("Model_likes_unlikes");
        $this->load->model('Model_user');
        $data['getLikes'] = $this->Model_likes_unlikes->getLikes($id);
        $data['getUnlikes'] = $this->Model_likes_unlikes->getUnlikes($id);
        $data['getForum'] = $this->Model_user->getForumById($id);
        $data['reply'] = $this->Model_user->getReply($id);
        $this->load->view('reply', $data);
    }

    #Insert Reply
    public function insertReply($id) {
        $data = array(
            'id_reply' => $this->generateID(10),
            'id_forum' => $id,
            'deskripsi' => $this->input->post('desc_reply'),
            'waktu' => date('Y-m-d H:i:s'),
            'id_akun' => $this->session->userdata('id_akun')
        );

        $this->load->model("Model_user");
        $query = $this->Model_user->insertReply($data);
        redirect('User/getReply/'.$id);
    }

    public function reply() {
        $data['reply'] = $this->Model_user->getReply($id);
        if ($data == null) {
            $data = "Tidak ditemukan komentar";
            $this->load->view('reply', $data);
        }
        else {
            $this->load->view('reply', $data);
        }
    }

    #Set Favorit
    public function setFavorit($id_forum) {
        $this->load->model("Model_user");
        $query = $this->Model_user->setFavorit($id_forum, '1');
        redirect('User/daftarFavorit');
    }

    #Unset Favorit
    public function unsetFavorit($id_forum) {
        $this->load->model("Model_user");
        $query = $this->Model_user->setFavorit($id_forum, '0');
        redirect('User/daftarFavorit');
    }

    #Daftar Favorit
    public function daftarFavorit() {
        $this->load->model("Model_user");
        $data['favorit'] = $this->Model_user->getFavorit();
        $this->load->view('daftarFavorit', $data);
    }

    #Set Likes
    public function setLikes($id_forum) {
        $data = array(
            'id' => $this->generateID(20),
            'id_forum' => $id_forum,
            'id_akun' => $this->session->userdata('id_akun'),
            'suka' => '1',
            'tidak_suka' => '0'
        );

        $this->load->model("Model_likes_unlikes");
        $query = $this->Model_likes_unlikes->insertLikesUnlikes($data);
        
        redirect('User/getAllLikesUnlikes/'. $id_forum);
    }

    #Set Unlikes
    public function setUnlikes($id_forum) {
        $data = array(
            'id' => $this->generateID(20),
            'id_forum' => $id_forum,
            'id_akun' => $this->session->userdata('id_akun'),
            'suka' => '0',
            'tidak_suka' => '1'
        );

        $this->load->model("Model_likes_unlikes");
        $query = $this->Model_likes_unlikes->insertLikesUnlikes($data);
        
        redirect('User/getAllLikesUnlikes/'. $id_forum);
        
    }

    public function getAllLikesUnlikes($id_forum) {
        $this->load->model("Model_likes_unlikes");
        $this->load->model("Model_user");
        $data['getLikes'] = $this->Model_likes_unlikes->getLikes($id_forum);
        $data['getUnlikes'] = $this->Model_likes_unlikes->getUnlikes($id_forum);
        $data['getForum'] = $this->Model_user->getForumById($id_forum);
        $data['reply'] = $this->Model_user->getReply($id_forum);
        $this->load->view('reply', $data);
    }
    
}

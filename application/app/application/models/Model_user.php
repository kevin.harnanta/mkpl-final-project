<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {
    #Insert Forum
    public function insertForum($data) {
        $this->db->insert('forum', $data);
    }

    #Get Forum
    public function getForum() {
        $query = $this->db->query("select * from forum, akun where forum.id_akun = akun.id_akun order by waktu desc");
        return $query->result_array();
    }

    #Count Forum
    public function getCountForum() {
        $query = $this->db->query("select count(*) from forum group by forum.id_akun");
        return $query->result_array();
    }

    #Get Forum
    public function getForumById($id) {
        $query = $this->db->query("select * from forum, akun where id_forum = '". $id . "'" . "and forum.id_akun = akun.id_akun");
        return $query->result_array();
    }

    #Get All Reply
    public function getReply($id) {
        $query = $this->db->query("select * from forum, posting, akun where forum.id_forum = posting.id_forum and forum.id_forum ='".$id."'" . "and posting.id_akun = akun.id_akun order by posting.waktu asc");
        return $query->result_array();
    }

    #Insert Reply
    public function insertReply($data) {
        $this->db->insert('posting', $data);
    }

    #Set Unset Favorit
    public function setFavorit($id_forum, $kategori) {
        $this->db->set('favorite', $kategori);
        $this->db->where('id_forum', $id_forum);
        $this->db->update('forum');
    }

    #Get Favorit
    public function getFavorit() {
        $query = $this->db->query("select * from forum , akun where forum.favorite = '1' and forum.id_akun = akun.id_akun");
        return $query->result_array();
    }
}
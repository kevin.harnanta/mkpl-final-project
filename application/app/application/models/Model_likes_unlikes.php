<?php

class Model_likes_unlikes extends CI_Model {

#Menambah likes sesuai dengan id_forum tertentu
    
    public function getLikes($id_forum) {
        $query = $this->db->query("select *, count(*) as total_suka from tbl_forum_suka_tidak where suka = '1' and id_forum = '". $id_forum . "'" . "group by id_forum");
        return $query->result_array();
    }

#Menambah unlikes sesuai dengan id_forum tertentu    
    public function getUnlikes($id_forum) {
        $query = $this->db->query("select *, count(*) as total_tidak_suka from tbl_forum_suka_tidak where tidak_suka = '1' and id_forum = '". $id_forum . "'" . "group by id_forum");
        return $query->result_array();
    }

#Mengecek likes dari daftar DB
    public function checkLikes() {
        $query = $this->db->query("select *, count(*) as suka from tbl_forum_suka_tidak where suka = '1' group by id_forum");
        return $query;
    }

#Mengecek unlikes dari daftar DB
    public function checkUnlikes() {
        $query = $this->db->query("select *, count(*) as tidak_suka from tbl_forum_suka_tidak where tidak_suka = '1' group by id_forum");
        return $query;
    }

#Update nilai likes dan unlikes dari id_forum dan id_akun
    public function setLikesUnlikes($id_forum, $likes, $unlikes, $id_akun) {
        $this->db->set('suka', $likes);
        $this->db->set('tidak_suka', $unlikes);
        $this->db->where('id_forum', $id_forum);
        $this->db->where('id_akun', $id_akun);
        $this->db->update('tbl_forum_suka_tidak');
    }

#Menambah Likes dan Unlikes
    public function insertLikesUnlikes($data) {
        $this->db->insert('tbl_forum_suka_tidak', $data);
    }

#Menghapus nilai likes dan unlikes dari id_forum dan id_akun
    public function deleteLikesUnlikes($id_forum, $id_akun) {
        $this->db->where('id_forum', $id_forum);
        $this->db->where('id_akun', $id_akun);
        $this->db->delete('tbl_forum_suka_tidak');
    }

}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_home extends CI_Model {
    #Insert Akun
	public function insertAccount($data) {
        $this->db->insert('akun', $data);
    }

    #Get Akun
    public function getAccount() {
        $query = $this->db->get("akun");
        return $query;
    }
}